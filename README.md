Paketbau für SL7 mit Docker
===========================

Docker bietet sich prima für den Bau von rpm-Paketen an, da für jedes Paket ein
temporärer Container gestartet werden kann, in dem alle Abhängigkeiten installiert
werden. Dadurch wird z.B. sichergestellt, dass im `SPEC`-File keine Abhängigkeiten
vergessen wurden.

Docker-Image vorbereiten
-------------------------

Diese Schritte sind nur einmalig auf einem Rechner notwendig. Sie dienen dazu,
ein SL7-Basis-Image zu erstellen, welches für alle weiteren Paketbauvorgänge
genutzt werden kann.

1. Docker installieren:<br>
   ```sudo yum install docker```

2. Image erstellen:<br>
   ```docker build -t paketbau https://gitlab.hrz.tu-chemnitz.de/klada--tu-chemnitz.de/paketbau.git#master:sl7```
   
3. Temporäres geteiltes Verzeichnis anlegen:<br>
   ```mkdir /tmp/rpmbuild-docker```

Container starten
-----------------

1. Temporäres geteiltes Verzeichnis anlegen:<br>
   ```mkdir /tmp/rpmbuild-docker```
2. Ein SRPM in den geteilten Ordner kopieren:<br>
   ```cp ~/Downloads/beispiel-1.0.src.rpm /tmp/rpmbuild-docker```
3. Berechtigungen setzen:<br>
   ```chmod -R a+rwx /tmp/rpmbuild-docker```
4. Temporären Container starten/betreten:<br>
   ```docker run --rm -v /tmp/rpmbuild-docker/:/home/user/rpmbuild -i -t paketbau /bin/bash```
5. Build-Dependencies installieren (im Container):<br>
   ```[root@IMGID /]# yum-builddep /home/user/rpmbuild/beispiel-1.0.src.rpm```
6. Paket bauen (auch im Container):<br>
   ```[root@IMGID /]# su - user
   [user@IMGID ~]$ rpm -i /home/user/rpmbuild/beispiel-1.0.src.rpm
   [user@IMGID ~]$ rpmbuild -ba /home/user/rpmbuild/SPECS/*.spec
   ```

Beim Verlassen des Containers wird dessen Inhalt dann automatisch zerstört
(durch die Option `--rm`). Die gebauten Pakete hingegen liegen auf dem Docker-Host
unter `/tmp/rpmbuild-docker`.